const express = require('express')
const { getItems, getItem, createItem, updateItem, deleteItem } = require('../controllers/users.controller')
const router = express.Router()
const checkOrigin = require('../middlewares/origen.middleware')

//TODO: localhost/users

router.get('/', checkOrigin, getItems)
router.get('/:id',checkOrigin, getItem)
router.post('/', checkOrigin, createItem)
router.put('/', checkOrigin, updateItem)
router.delete('/:id', checkOrigin, deleteItem)

module.exports = router