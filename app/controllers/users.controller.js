const { httpError } = require("../helpers/error.helper")
const userModel = require("../models/user.model")

const getItem = async (req, res) => {
    try {
        const id = req.params.id;
        const item = await userModel.findById(id)
        res.send({ data: item })
    } catch (error) {
        httpError(res, error)
    }
}

const getItems = async (req, res) => {
    try {
        const items = await userModel.find({})
        res.send({ data: items })
    } catch (error) {
        httpError(res, error)
    }
}

const createItem = async (req, res) => {
    try {
        const { name, username, email, password } = req.body
        const resDetail = await userModel.create({
            name, username, email, password
        })
        res.send({ data: resDetail })
    } catch (error) {
        httpError(res, error)
    }
}

const updateItem = (req, res) => {

}

const deleteItem = (req, res) => {

}

module.exports = {
    getItem, getItems, createItem, updateItem, deleteItem
}