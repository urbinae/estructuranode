const mongoose = require('mongoose')

const User = new mongoose.Schema({
    name: {
        type: String
    },
    username: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    }
},
    {
        timestamps: true,
        versionKey: false
    }
)

module.exports = mongoose.model('users', User)